#!/usr/bin/env bash

set -e

if [ $UID -gt 0 ];
then
        echo "You must be root to launch this"
        exit 1
fi

DB_NAME=$1
ENVIRONMENT=$2
PASS=$(pwgen 16 1)

case $ENVIRONMENT in
        dev)
                RANGE=10.208.128.0/17
                ;;
        preprod)
                RANGE=10.210.128.0/17
                ;;
        prod)
                RANGE=10.209.128.0/17
                ;;
        *)
                echo "Invalid environment. must be dev/preprod/prod"
                exit 1
                ;;
esac

sudo -u postgres psql -c "CREATE USER $DB_NAME WITH PASSWORD '$PASS' LOGIN;"
sudo -u postgres psql -c "CREATE DATABASE $DB_NAME WITH OWNER $DB_NAME TEMPLATE template1;"

echo "host    $DB_NAME         $DB_NAME         $RANGE         md5" >> /etc/postgresql/12/main/pg_hba.conf
sudo pg_ctlcluster 12 main reload

echo "Created $DB_NAME with $PASS
